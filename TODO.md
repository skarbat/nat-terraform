## TO DO list

Below is a list of things to do to improve and/or fix known issues.

 * Parametrize the Role, vpc and possibly subnet id.
 * Expand tests to make sure EC2 client reaches internet throught the NAT
 * Decouple the ssh key - currently shared amongst NAT and client EC2 instances

