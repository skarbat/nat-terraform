#
# Makefile
#
# Creates deploys and tests the AWS NAT Instance
#

SSH_KEYFILE_NAT:=nat-ssh

.PHONY: all init plan apply output destroy tests

all:
	@echo "make: init, plan, apply, output, destroy, tests"

ssh-keys: $(SSH_KEYFILE_NAT)
$(SSH_KEYFILE_NAT):
	ssh-keygen -t rsa -P "" -f $(SSH_KEYFILE_NAT)

init: ssh-keys
	cd terraform && terraform init
plan:
	cd terraform && terraform plan
apply:
	cd terraform && terraform apply
output:
	cd terraform && terraform output -json
destroy:
	cd terraform && terraform destroy
tests:
	pytest-3 -v tests
