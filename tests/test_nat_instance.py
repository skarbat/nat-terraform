#
#  pytest module to test the NAT instance deployment.
#

import pytest
import json
import os

class TestNAT:
    @pytest.fixture()
    def setUp(self):
        self.ssh_key = 'nat-ssh'
        self.username = 'ec2-user'
        self._get_terraform_ip()

    def _get_terraform_ip(self):
        data = os.popen('cd terraform ; terraform output -json')
        self.values = json.loads(data.read())
        self.nat_ip = self.values['nat_public_ip']['value']

    def _ssh_command(self, command):
        return os.popen('ssh -o "StrictHostKeyChecking no" -i {} {}@{} {}'.format(
            self.ssh_key, self.username, self.nat_ip, command)).read()

    def test_terraform_values(self, setUp):
        assert self.values is not None
        assert self.nat_ip is not None

    def test_ssh_nat_client_http(self, setUp):
        data = os.popen('curl {}'.format(self.nat_ip)).read()
        assert data is not None
        assert data.find ('Test Page for the Apache HTTP Server') is not -1

    def test_ssh_nat_instance_uptime(self, setUp):
        data = self._ssh_command('uptime')
        assert data is not None
        assert data.find ('load average') is not -1

    def test_nat_instance_uptime(self, setUp):
        data = self._ssh_command('uptime')
        assert data is not None
        assert data.find ('load average') is not -1

    def test_nat_instance_dual_ifaces(self, setUp):
        data = self._ssh_command('sudo ip a')
        assert data is not None
        assert data.find ('eth0') is not -1
        assert data.find ('eth1') is not -1
